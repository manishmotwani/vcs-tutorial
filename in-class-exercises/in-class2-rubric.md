# Advanced Uses of Git

Total: 25 points

- 12pts for questions (2pt each)

- 13pts for other deliverables:
    
    - 3pts: visualization
    
    - 10 pts: repo

Quesitons should be judged on comprehension. Correct terminology should be used.
There is not a lot of wiggle room (2 pts/each) so I would allot all 2 points for
a good answer and mark off for:

* Misuse of terminology (-0.5 pts)
* Misunderstanding of concepts/partially incorrect (up to -2, your discretion)

Language is really important in Git so ensuring that students know that they are
misusing terms is important. Graders must convey this to the students while marking
off any points ("You are confusing add and commit.", for example)

Ensure that for a given question (especially the ones that require explanation), 
you deduct same points for same/similar mistakes for all the students. 

You should be able to run git commands from inside the folders extracted from
the repository files submitted by the students. Note that there is a difference
between *repository* and *working directory* so for switching the branch use
command `git symbolic-ref HEAD refs/heads/<mybranch>` 
(src: https://feeding.cloud.geek.nz/posts/setting-default-git-branch-in-bare/).
Remaining git commands will work fine inside the extracted repositories. 

# Detailed rubric for each question:

**Questions: total 12 points (award 2 points for each question)**

Q1.  How many commits did you cherry-pick?
- Give 1.5 points to a number that is +/-1 to the correct answer (14). Students do not need to list 
the hash of the commits. 

Q2. Are the commit hashes of the cherry-picked commits identical in master and feature-branch?
Briefly explain why.
- This has 2 parts. First part is yes/no answer so award 0.5 points if correct. 
  Second part is explanation so award 1.5 points if explanation is correct. The correct explanation
  must be something like "the cherry-picked commits are not identical with the original ones because 
  they have different timestamps and parents’ relationship as well as other meta data."
  If the answer is partially correct then deduct upto 1 point depending on the correctness of the answer. 

Q3. What happens if you merge a branch from which you previously cherry-picked single commits? How
often do the cherry-picked commits appear in the history? Briefly explain why.
- This has 3 parts. First part requires explanation so award 1 point for correct explanation. 
  If the students miss out telling that a new commit is created after merging (see correct answer in 
  in-class1-solutions.txt) then deduct 1 point. 
  Second part is numeric answer so award 0.5 points if answer is correct (answer is twice).
  Third part is explanation so award 0.5 points for correct explanation. The correct answer is 
  "These branches remain unchanged so that two copies of the cherry-picked commit objects will exist."
  If the explanation is partially correct then deduct 0.25 points. 

Q4. What are the risks of rebasing? Briefly describe a use case in which rebasing can be safely applied.
- This has 2 parts and both are descriptive. Award 1 point for each part. The answer of first part should
  contain two risks of rebasing - getting merge conflict and losing information permanently (see answer 
  in in-class1-solutions.txt). If one of these is not covered then deduct 0.5 points. 
  The most common answer for second part is using rebase in a local repo for commits which are not yet pushed. 
  Most of the students answer it correctly but some of them do not. See answers for detailed use-cases.      

Q5. What are the risks of using reset when a commit has already been pushed?
- Award 2 points if the answer is correct. The same 2 risks of rebasing apply to resettting as well. 
  Use the same grading criteria as you use for Q4 part 1 above. 
 
Q6.  Does revert remove the reverted commit? Briefly explain how revert works.
- This has 2 parts. First part is yes/no answer so award 0.5 points if answer is correct (correct ans is no).
  Award 1.5 points for the second part if the explanation is correct.  The correct answer should say something
  similar to "Revert makes a _new_ commit object w/ identical content to a previous commit but with a different 
  parent commit object." If the answer is partially correct then deduct upto 1 points based on the correctness
  of the answer. 
    
**Deliverables: total 13 points**

D1. answers.txt (2 point)

D2. relationships (3 points) (screen-shot or copy pasting the tree-structured output of _git log_ commamd or manually drawn diagrams using external tools is OK)
Make sure that these show commit hashes and verify a few of the hashes against the commit logs in the repositories.   

D3. basic-stats repo 

D4. basic-stats-fork repo
Ensure that content inside (commit hashes for instance) answers.txt file match the content in D3 and D4  (2 points)

Branching and cherry-picking (2 points)

Rebasing (2 points)

Resetting and reverting (2 points)
