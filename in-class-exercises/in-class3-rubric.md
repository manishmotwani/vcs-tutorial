# Git Bisect --- In Class 3

Total: 100 points

- 70 points for answers (10pt each)
- 25 points for identifying correct defect-inducing change (10 points for identifying the defect-inducing change,15 points for submitting the diff/buggy source code)
- 5 points for correct deliverables


**Answers: total 70 points**
Each question is for 10 points. Award full points if answer is correct. 
For questions which have two parts (3,4, and 6) award 5 points for each part.  

**Defect-inducing change: total 25 points**
The defect-inducing line is the incorrect assignment inside the second for statement 
within the bubbleSort function. Award 10 points if students are able to isolate and 
describe this defect. 

- Students are required to submit either a diff file or the buggy source code 
that should contain *only* the defect-inducing change. 
- If a diff file is submitted then try to apply the changes to the latest version which 
passes all the tests and see if the patched file now failes the test. 
- If a buggy source code is submitted then verify that diff between buggy source code 
and the latest version that passes all the tests is *only* the defect-inducing change. 
Also, verify that buggy source code fails the tests.   
Award 15 points if the diff or buggy source code reflects the correct defect inducing 
change. 


**Deliverables: total 5 points**
D1. submission file name satisfies <group name>-inclass2.<zip/tar.gz> (2.5 points) 
D2. answers.txt lists all group members on top of this file. (2.5 point)

