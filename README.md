This repository contains the Reusable Learning Object (RLO) developed to teach about Version Control Systems.

Repository link: https://bitbucket.org/manishmotwani/vcs-tutorial/src/master/

Download the repository in compressed file from [here](https://bitbucket.org/manishmotwani/vcs-tutorial/get/3d7571ebcec9.zip)

The target audience for this learning are Computer Science students (both under-graduate and graduate). 

The RLO consists of:

* Lecture slides describing the basics of version control system.

* Three In-class exercises (in-class1 for beginners and in-class2 and in-class3 for advanced students) targeted to give hands-on experience to the students for using **git**.  


To run the jupyter notebook files kindly follow the following steps.

1. Install Python (version 3 recommended)

2. Install Jupyter Notebook (http://jupyter.org/install) 

3. Install Pandoc (http://pandoc.org/installing.html) (optional) this is used for exporting Jupyter Notebook file into various other formats including LaTeX, Pdf, Html etc. 

4. From the terminal run the command: `jupyter notebook`

5. A window will open up in a browser listing all the files and folders on your machine. 

6. Browse to the current folder (<path-to-vcs-tutorial>/in-class-exercises/) and click on the ".ipynb" file.

7. This will open the file in new window. 

8. You can run all embedded code snippets (cells) by a single click on "double arrow" icon in the tool bar at the top OR 
   run a individiual code snippet using the "single arrow" icon on the left size of the cell. 
